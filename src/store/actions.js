import Vue from 'vue'
import {
  reqSongUrl,
  reqSongInfo,
  reqSongLyric,
  reqAlbumListInfo
} from '@/api/index.js'

import {
  reqTopListInfo
} from '@/api/toplist.js'


import {
  ADD_PLAY_SONG,
  UPDATE_PLAY_SONGS
} from './mutations-type'


export default {
  // 封装 一首歌的信息
  async song(_, id) {
    const res1 = await reqSongUrl(id)
    let playSong1 = {
      // id: res1.data.data[0].id,
      url: res1.data.data[0].url
    }

    const res2 = await reqSongLyric(id)
    let playSong2 = {}
    if (res2.data.lrc) {
      playSong2 = {
        lrc: res2.data.lrc.lyric
      }
    } else {
      playSong2 = {
        lrc: '暂无歌词'
      }
    }
    return Object.assign(playSong1, playSong2)
  },

  //请求单曲的全部信息
  async reqSong({
    commit
  }, id) {
    const res1 = await reqSongUrl(id)
    let playSong1 = {
      id: res1.data.data[0].id,
      url: res1.data.data[0].url
    }

    const res2 = await reqSongInfo(id)
    let playSong2 = {
      name: res2.data.songs[0].name,
      cover: res2.data.songs[0].al.picUrl,
      artist: res2.data.songs[0].ar[0].name
    }

    const res3 = await reqSongLyric(id)
    let playSong3 = {
      lrc: res3.data.lrc.lyric
    }
    let playSong = Object.assign(playSong1, playSong2, playSong3)
    commit(ADD_PLAY_SONG, {
      playSong
    })
  },

  //请求歌单列表
  async reqPlaySongs({
    dispatch,
    commit
  }, id) {
    let playSongs = []
    let res = await reqTopListInfo(id)
    res = res.data.playlist.tracks
    for (const item of res) {
      let resObj = {}
      resObj.id = item.id
      resObj.name = item.name
      resObj.artist = item.ar[0].name
      resObj.cover = item.al.picUrl
      /* 异步处理 -- 使用.then()方法来处理 res: 歌url / 歌词；playSongs:目标歌单  -- */
      dispatch('song', item.id).then(res => {
        resObj = {
          ...res,
          ...resObj,
        }
        playSongs.push(resObj)
      })
    }
    Vue.nextTick(() => {
      commit(UPDATE_PLAY_SONGS, {
        playSongs
      })
    })

  },

  //请求新碟列表
  async reqPalyAlbum({
    dispatch,
    commit
  }, id) {
    let resArr = []
    let res = await reqAlbumListInfo(id)
    res = res.data.songs

    // 唱片实际不止一首歌（有原唱版，伴奏版，其它..），此处暂定为一首歌 
    res.map((item, index, arr) => {
      let resObj = {}
      resObj.id = item.id
      resObj.name = item.name
      resObj.artist = item.ar[0].name
      resObj.cover = item.al.picUrl
      // 错误原因：不是 歌的url(歌词)对应的 id （暂时没有找到到对应的id）
      dispatch('song', item.id).then(res => {
        resObj = Object.assign(resObj, res)
        resArr.push(resObj)
        if (index + 1 === arr.length) {
          return resArr
        }
      })


    })
    let playSongs = resArr

    Vue.nextTick(() => {
      commit(UPDATE_PLAY_SONGS, {
        playSongs
      })
    })

  }
}