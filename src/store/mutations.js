import {
  ADD_PLAY_SONG,
  UPDATE_PLAY_SONGS
} from './mutations-type'

export default {
  //添加单曲播放列表
  [ADD_PLAY_SONG](state, {
    playSong
  }) {
    state.playSong = playSong
  },
  [UPDATE_PLAY_SONGS](state, {
    playSongs
  }) {
    state.playSongs = playSongs
  }
}