import Vue from 'vue'
import VueRouter from 'vue-router'
import Discover from '@/views/Discover/Discover.vue'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    redirect: Discover
  },
  {
    path: '/my',
    name: 'My',
    meta: {
      name: '我的音乐'
    },
    components: {
      mainArea: () => import('@/views/My/My')
    },
  },
  {
    path: '/friend',
    name: 'Friend',
    meta: {
      name: '朋友'
    },
    components: {
      mainArea: () => import('@/views/Friend/Friend')
    },
  },
  {
    path: '/download',
    name: 'Download',
    meta: {
      name: '下载客户端'
    },
    components: {
      mainArea: () => import('@/views/Download/Download')
    },
  },
  {
    path: '/discover',
    name: 'Discover',
    meta: {
      name: '主页-发现音乐'
    },
    components: {
      /* 使用components-- name:组件（name是路由出口的name）  /  default:component  ||  原写法 -- component: Discover,*/
      mainArea: Discover
    },
  },
  {
    path: '/discover/toplist',
    name: 'TopList',
    meta: {
      name: '排行榜-HD音乐'
    },
    components: {
      mainArea: () => import('@/views/Discover/TopList/TopList.vue')
    }
  },
  {
    path: '/discover/playlist',
    name: 'PlayList',
    meta: {
      name: '歌单-HD音乐'
    },
    components: {
      mainArea: () => import('@/views/Discover/PlayList/PlayList.vue')
    }
  },
  {
    path: '/discover/djradio',
    name: 'DjRadio',
    meta: {
      name: '主播电台-HD音乐'
    },
    components: {
      mainArea: () => import('@/views/Discover/DjRadio/DjRadio.vue')
    }
  },
  {
    path: '/discover/artist',
    name: 'Artist',
    meta: {
      name: '歌手-HD音乐'
    },
    components: {
      mainArea: () => import('@/views/Discover/Artist/Artist.vue')
    }
  },
  {
    path: '/discover/album',
    name: 'Album',
    meta: {
      name: '新碟上架-HD音乐'
    },
    components: {
      mainArea: () => import('@/views/Discover/Album/Album.vue')
    }
  },
  {
    path: '/search',
    name: 'Search',
    meta: {
      name: '搜索结果'
    },
    components: {
      mainArea: () => import('@/views/Search/Search.vue')
    }
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      name: '登录-HD音乐'
    },
    // 懒加载路由.
    components: {
      mainArea: () => import('@/views/Login/Login.vue')
    }
  }
]

const router = new VueRouter({
  routes
})

//更改网页标题
router.afterEach((to) => {
  document.title = to.meta.name
})

export default router