// //认证文件  -- 使用js-cookie，是一个简单的，轻量级的处理cookies的js API。
// // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
// //import Cookies from 'js-cookie'

// const TokenKey = 'Admin-Token'

// export function getToken() {
//   return Cookies.get(TokenKey)
// }

// export function setToken(token) {
//   return Cookies.set(TokenKey, token)
// }

// export function removeToken() {
//   return Cookies.remove(TokenKey)
// }