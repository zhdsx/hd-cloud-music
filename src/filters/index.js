import Vue from 'vue'
import {
  format
} from 'date-fns'
// [Vue warn]: Failed to resolve filter: filterPlayCount
Vue.filter('filterPlayCount', (value) => {
  if (!value) return ''
  if (parseInt(value) > 100000000) {
    return Math.floor(value / 100000000) + Math.floor((value - 100000000) / 10000000) * 0.1 + '亿'
  } else if (parseInt(value) > 10000) {
    return Math.floor(value / 10000) + '万'
  } else if (parseInt(value) < 10000) {
    return value
  }
})
Vue.filter('filterPlayTime', (value, formatStr = 'yyyy-MM-dd HH:mm:ss') => {
  if (!value) return ''
  return format(value, formatStr);
})