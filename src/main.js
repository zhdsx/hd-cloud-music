import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './styles/normalize.css'
import App from './App.vue'
import router from './router'
import store from './store'
import APlayer from '@moefe/vue-aplayer';
import './filters/index.js'
import VueLazyload from 'vue-lazyload'


Vue.config.productionTip = false

Vue.use(ElementUI);
Vue.use(APlayer, {
  defaultCover: require("./assets/images/one.jpg"),
  productionTip: true,
});
Vue.use(VueLazyload, {
  error: require("./assets/gif/oneall.gif"),
  loading: require("./assets/gif/luffy.gif"),

})



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')