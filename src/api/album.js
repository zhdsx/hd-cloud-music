import request from './request'


/* 
新碟上架
说明 : 调用此接口 , 可获取新碟上架列表 , 如需具体音乐信息需要调用获取专辑列表接 口 /album , 然后传入 id, 如 /album?id=32311&limit=30
可选参数 :
  limit: 取出数量 , 默认为 50
  offset: 偏移数量 , 用于分页 , 如 :( 页数 -1)*50, 其中 50 为 limit 的值 , 默认 为 0
  area: ALL:全部,ZH:华语,EA:欧美,KR:韩国,JP:日本
  type : new:全部 hot:热门,默认为 new
  year : 年,默认本年
  month : 月,默认本月
接口地址 : /top/album
调用例子 : /top/album?offset=0&limit=30&year=2019&month=6
*/
export async function reqNewAlbumList(limit = 10) {
  const res = await request({
    url: `/top/album?limit=${limit}`
  })
  return res
}


/* 
全部新碟
说明 : 登录后调用此接口 ,可获取全部新碟
可选参数 :
limit : 返回数量 , 默认为 30
offset : 偏移数量，用于分页 , 如 :( 页数 -1)*30, 其中 30 为 limit 的值 , 默认为 0
area : ALL:全部,ZH:华语,EA:欧美,KR:韩国,JP:日本
接口地址 : /album/new
调用例子 : /album/new?area=KR&limit=10
*/
export async function reqAllAlbumList(page = 1, area = 'ALL', limit = 35) {
  const res = await request({
    url: `/album/new?area=${area}&offset=${(page-1)*limit}&limit=${limit}`
  })
  return res
}