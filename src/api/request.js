import axios from 'axios'
import {
  Message
} from 'element-ui'
import store from '@/store'
import {
  getToken
} from '@/utils/auth'

// 基础路径
const BASE_URL = '/api'

// 创建axios实例
const service = axios.create({
  baseURL: BASE_URL, // api的base_url
  timeout: 50000 // 请求超时时间
  /* 一般baseURL这么写
    baseURL: process.env.BASE_API, 
    解释：
    api的baseURL 首先我们在根目录新建3个文件，分别为.env.development，.env.production，.env.test
    .env.development 模式用于serve，开发环境，就是开始环境的时候会引用这个文件里面的配置
    .env.production模式用于build，线上环境。
    .env.test 测试环境
    都是设置：VUE_APP_BASE_API = '需要请求API'
  */
})

// request请求拦截器
service.interceptors.request.use(config => {
  if (store.getters.token) {
    config.headers['X-Token'] = getToken() // 让每个请求携带token--['X-Token']为自定义key 请根据实际情况自行修改
  }
  return config
}, error => {
  console.log(error) // for debug
  Promise.reject(error)
})

// respone响应拦截器
service.interceptors.response.use(
  response => response,
  error => {
    console.log('err' + error) // for debug
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  })

export default service