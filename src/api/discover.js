//  发现音乐 > 推荐（主页）

import request from './request'

// 首页左边
/* 
  轮播图 
  接口地址：/banner
  type:资源类型,对应以下类型,默认为 0 即PC
    0: pc
    1: android
    2: iphone
    3: ipad
*/
export async function reqBanner() {
  const res = await request({
    url: '/banner'
  })
  return res
}

/* 
  (热门)推荐歌单
  接口地址 : /personalized
  可选参数 : limit: 取出数量 , 默认为 30 (不支持 offset)
  调用例子 : /personalized?limit=1
*/
export async function reqRecommendPlayList() {
  const res = await request({
    url: '/personalized?limit=8'
  })
  return res
}


/* 
  新碟上架
  说明 : 调用此接口 ，获取云音乐首页新碟上架数据
  接口地址 : /album/newest
  调用例子 : /album/newest
*/
export async function reqNewAlbum() {
  const res = await request({
    url: `/album/newest`
  })
  return res
}

/* 
  所有榜单
  接口地址 : /toplist
  调用例子 : /toplist
*/
export async function reqAll() {
  const res = await request({
    url: '/toplist'
  })
  return res
}

/* 
  所有榜单 > 歌单详情（飙升榜、新歌榜、原创榜--前10）
  接口地址 : /playlist/detail
  必选参数 : id : 歌单 id
  可选参数 : s : 歌单最近的 s 个收藏者,默认为8
  调用例子 : /playlist/detail?id=24381616
*/
export async function reqTopList(id) {
  let res = await request({
    url: `/playlist/detail?id=${id}`
  })
  res = res.data.playlist.tracks.slice(0, 10)
  return res
}

// 首页右边
/* 
  热门歌手(入驻音乐人)
  说明 : 调用此接口 , 可获取热门歌手数据
  接口地址 : /top/artists
  可选参数 : 
    limit: 取出数量 , 默认为 50
    offset: 偏移数量 , 用于分页 , 如 :( 页数 -1)*50, 其中 50 为 limit 的值 , 默认 为 0
  调用例子 : /top/artists?offset=0&limit=30
*/
export async function reqHotSinger() {
  const res = await request({
    url: "/top/artists?offset=0&limit=5"
  })
  return res
}

/* 
  热门主播
  接口地址 : /dj/toplist/popular
  可选参数 
    limit : 返回数量 , 默认为 100 (不支持 offset)
  调用例子 : /dj/toplist/popular?limit=30
*/
export async function reqHotDj() {
  const res = await request({
    url: "/dj/toplist/popular?limit=5"
  })
  return res
}