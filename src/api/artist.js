import request from './request'

/* 
歌手分类列表
说明 : 调用此接口,可获取歌手分类列表
可选参数 :
limit : 返回数量 , 默认为 30
offset : 偏移数量，用于分页 , 如 : 如 :( 页数 -1)*30, 其中 30 为 limit 的值 , 默认为 0
initial: 按首字母索引查找参数,如 /artist/list?type=1&area=96&initial=b 返回内容将以 name 字段开头为 b 或者拼音开头为 b 为顺序排列, 热门传-1,#传0
type 取值:
  -1:全部
  1:男歌手
  2:女歌手
  3:乐队
area 取值:
  -1:全部
  7华语
  96欧美
  8:日本
  16韩国
  0:其他
接口地址 : /artist/list
调用例子 : /artist/list?type=1&area=96&initial=b /artist/list?type=2&area=2&initial=b
*/
export async function reqArtistList({
  initial = -1,
  area = -1,
  type = -1,
  page = 1,
  limit = 20
}) {
  const res = await request({
    url: `/artist/list?initial=${initial}&area=${area}&type=${type}&offset=${(page-1)*limit}&limit=${limit}`
  })
  return res
}


/* 
热门歌手(歌手榜)
说明 : 调用此接口 , 可获取排行榜中的歌手榜
可选参数 :
type : 地区
  1: 华语
  2: 欧美
  3: 韩国
  4: 日本
接口地址 : /toplist/artist
调用例子 : /toplist/artist
*/
export async function reqHotArtists() {
  const res = await request({
    url: `/toplist/artist`
  })
  return res
}