import request from './request'

/* 
电台 - 分类
说明 : 登录后调用此接口 , 可获得电台类型
接口地址 : /dj/catelist
调用例子 : /dj/catelist
*/
export async function reqDjCatelist() {
  const res = await request({
    url: `/dj/catelist`
  })
  return res
}

/* 
电台个性推荐
  说明 : 调用此接口,可获取电台个性推荐列表 可选参数 :
  limit : 返回数量,默认为 6,总条数最多6条
  接口地址 : /dj/personalize/recommend
  调用例子 : /dj/personalize/recommend?limit=5
*/
export async function reqDjRecommend() {
  const res = await request({
    url: `/dj/personalize/recommend?limit=4`
  })
  return res
}


/* 
电台 - 付费精品
  说明 : 调用此接口,可获取付费精品电台
  可选参数 :
  limit : 返回数量 , 默认为 100 (不支持 offset)
  接口地址 : /dj/toplist/pay
  调用例子 : /dj/toplist/pay?limit=30
*/
export async function reqDjPay() {
  const res = await request({
    url: `/dj/toplist/pay?limit=4`
  })
  return res
}


/* 
电台 - 24小时节目榜
说明 : 调用此接口,可获取24小时节目榜
可选参数 :
limit : 返回数量 , 默认为 100 (不支持 offset)
接口地址 : /dj/program/toplist/hours
调用例子 : /dj/program/toplist/hours?limit=1
*/
export async function reqDjHours() {
  const res = await request({
    url: `/dj/program/toplist/hours?limit=4`
  })
  return res
}

/* 
电台 - 主播新人榜
说明 : 调用此接口,可获取主播新人榜
可选参数 :
limit : 返回数量 , 默认为 100 (不支持 offset)
接口地址 : /dj/toplist/newcomer
调用例子 : /dj/toplist/newcomer?limit=30
*/
export async function reqDjNewComer() {
  const res = await request({
    url: `/dj/toplist/newcomer?limit=4`
  })
  return res
}

/* 
电台 - 最热主播榜
说明 : 调用此接口,可获取最热主播榜
可选参数 :
limit : 返回数量 , 默认为 100 (不支持 offset)
接口地址 : /dj/toplist/popular
调用例子 : /dj/toplist/popular?limit=30
*/
export async function reqDjPopular() {
  const res = await request({
    url: `/dj/toplist/popular?limit=4`
  })
  return res
}