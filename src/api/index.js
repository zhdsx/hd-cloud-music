import request from './request'

/* 
## 获取音乐 url
  说明 : 使用歌单详情接口后 , 能得到的音乐的 id, 但不能得到的音乐 url, 调用此接口, 传入的音乐 id( 可多个 , 用逗号隔开 ), 可以获取对应的音乐的 url,未登录状态或者非会员返回试听片段(返回字段包含被截取的正常歌曲的开始时间和结束时间)
  注 : 部分用户反馈获取的 url 会 403,hwaphon找到的解决方案是当获取到音乐的 id 后，将 https://music.163.com/song/media/outer/url?id=id.mp3 以 src 赋予 Audio 即可播放
  必选参数 : id : 音乐 id
  可选参数 : br: 码率,默认设置了 999000 即最大码率,如果要 320k 则可设置为 320000,其他类推
  接口地址 : /song/url
  调用例子 : /song/url?id=33894312 /song/url?id=405998841,33894312
*/
export async function reqSongUrl(id) {
  let res = await request({
    url: `/song/url?id=${id}`
  })
  return res
}


/* 
## 歌曲信息
  说明 : 调用此接口 , 传入音乐 id(支持多个 id, 用 , 隔开), 可获得歌曲详情
  必选参数 : ids: 音乐 id, 如 ids=347230
  接口地址 : /song/detail
  调用例子 : /song/detail?ids=347230,/song/detail?ids=347230,347231
*/
export async function reqSongInfo(id) {
  let res = await request({
    url: `/song/detail?ids=${id}`
  })
  return res
}

/* 
## 获取歌词
  说明 : 调用此接口 , 传入音乐 id 可获得对应音乐的歌词 ( 不需要登录 )
  接口地址 : /lyric
  必选参数 : id: 音乐 id
  调用例子 : /lyric?id=33894312
*/
export async function reqSongLyric(id) {
  let res = await request({
    url: `/lyric?id=${id}`
  })
  return res
}

/* 
## 获取歌单详情
  说明 : 歌单能看到歌单名字, 但看不到具体歌单内容 , 调用此接口 , 传入歌单 id, 可 以获取对应歌单内的所有的音乐(未登录状态只能获取不完整的歌单,登录后是完整的)，但是返回的trackIds是完整的，tracks 则是不完整的，可拿全部 trackIds 请求一次 song/detail 接口获取所有歌曲的详情 (https://github.com/Binaryify/NeteaseCloudMusicApi/issues/452)
  接口地址 : /playlist/detail
  必选参数 : id : 歌单 id
  可选参数 : s : 歌单最近的 s 个收藏者,默认为8
  调用例子 : /playlist/detail?id=24381616
*/
export async function reqPlayListInfo(id) {
  let res = await request({
    url: `/song/detail?ids=${id}`
  })
  return res
}

/* 
## 新碟上架
说明 : 
  调用此接口 , 可获取新碟上架列表 , 如需具体音乐信息需要调用获取专辑列表接 口 /album , 
然后传入 id, 如 /album?id=32311&limit=30
*/
export async function reqAlbumListInfo(id) {
  let res = await request({
    url: `/album?id=${id}`
  })
  return res
}