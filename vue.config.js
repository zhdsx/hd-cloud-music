const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}


module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "~@/assets/scss/main.scss";`,
      },
    }
  },
  devServer: { //代理
    proxy: {
      "/api": { // 匹配所有以 '/api'开头的请求路径
        target: "http://localhost:3000", //这里可以跟随项目实际部署服务器来 网易API：https://autumnfish.cn
        changeOrigin: true, // 支持跨域
        ws: true, //如果要代理 websockets，配置这个参数
        pathRewrite: {
          "^/api": "" // 重写路径: 去掉路径中开头的'/api'
        }
      }
    }
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
}