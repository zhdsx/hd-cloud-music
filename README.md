# 云音乐

## 初始化

npm install

## 运行

npm run serve

## 待完善功能：

1.登录相关，比如个人歌单，推荐，个人的点赞、收藏、评论等； 2.添加歌手详情，歌单详情，评论模块； 3.添加播放功能按钮,优化播放组件； 4.添加加载进度条。

## 后端 API

本项目后端所有数据来自开源项目 NeteaseCloudMusicApi

- 后端开源项目地址https://github.com/Binaryify/NeteaseCloudMusicApi；
- 相关文档https://neteasecloudmusicapi.vercel.app/#/
